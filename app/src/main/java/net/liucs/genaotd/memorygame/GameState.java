package net.liucs.genaotd.memorygame;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Random;

public class GameState implements Serializable {
    private static final long serialVersionUID = 0L;

    private static int REMOVED = 2;
    private static int FACE_UP = 1;
    private static int FACE_DOWN = 0;

    private int totalNumberOfCards;
    private int cardValues[];
    private int cardStates[];
    private int numRemoved;
    private int numFaceUp;

    public boolean isGameOver() {
        return numRemoved == totalNumberOfCards;
    }

    // from http://www.dotnetperls.com/shuffle-java
    static public void shuffle(int[] array) {
        int n = array.length;
        for (int i = 0; i < array.length; i++) {
            // Get a random index of the array past i.
            int random = i + (int) (Math.random() * (n - i));
            // Swap the random element with the present element.
            int randomElement = array[random];
            array[random] = array[i];
            array[i] = randomElement;
        }
    }

    public GameState(int level, int numPossibleCards) {
        switch(level) {
            case 1: totalNumberOfCards = 36; break; // 6x6
            default: totalNumberOfCards = 16; // 4x4
        }
        if(!(totalNumberOfCards/2 < numPossibleCards)) {
            throw new InvalidParameterException("The number of possible cards is too small.");
        }
        cardValues = new int[totalNumberOfCards];
        cardStates = new int[totalNumberOfCards];
        Random rng = new Random();
        ArrayList<Integer> chosenCards = new ArrayList<Integer>();
        for (int i = 0; i < totalNumberOfCards; i += 2) {
            int randomCardValue;
            do {
                randomCardValue = rng.nextInt(numPossibleCards);
            }
            while (chosenCards.contains(randomCardValue));
            chosenCards.add(randomCardValue);
        cardValues[i] = randomCardValue;
        cardValues[i + 1] = randomCardValue;
        }
        shuffle(cardValues);
    }

    public int getNumCards() {
        return totalNumberOfCards;
    }

    public boolean isCardRemoved(int i) {
        return cardStates[i] == REMOVED;
    }

    public boolean isCardFaceUp(int i) {
        return cardStates[i] == FACE_UP;
    }

    public int getCardValue(int i) {
        return cardValues[i];
    }

    private void show() {
        int i = 0;
        for(int row = 0; row < getRows(); row++) {
            for(int col = 0; col < getCols(); col++, i++) {
                if(isCardRemoved(i)) {
                    System.out.print("  --- ");
                }
                else if(isCardFaceUp(i)) {
                    System.out.printf("%5d ", cardValues[i]);
                }
                else {
                    System.out.print("  *** ");
                }
            }
            System.out.println();
        }
    }

    public boolean readyToMatch() {
        return numFaceUp == 2;
    }

    public int[] getFaceUps() {
        int[] ups = new int[numFaceUp];
        int j = 0;
        for(int i = 0; i < cardStates.length; i++) {
            if(cardStates[i] == FACE_UP) {
                ups[j] = i;
                j++;
            }
        }
        return ups;
    }

    public void cancelMove() {
        if(numFaceUp == 1) {
            for (int i = 0; i < totalNumberOfCards; i++) {
                if (isCardFaceUp(i)) {
                    cardStates[i] = FACE_DOWN;
                    numFaceUp--;
                    break;
                }
            }
        }
    }

    public boolean tryMatch() {
        if(!readyToMatch()) {
            throw new IllegalArgumentException("There must be 2 cards face up.");
        }
        int [] ups = getFaceUps();
        numFaceUp -= 2;
        if (cardValues[ups[0]] == cardValues[ups[1]]) {
            cardStates[ups[0]] = REMOVED;
            cardStates[ups[1]] = REMOVED;
            numRemoved += 2;
            return true;
        }
        else {
            cardStates[ups[0]] = FACE_DOWN;
            cardStates[ups[1]] = FACE_DOWN;
            return false;
        }
    }

    public int getRows() {
        return (int) Math.sqrt(totalNumberOfCards);
    }

    public int getCols() {
        return (int) Math.sqrt(totalNumberOfCards);
    }

    public boolean turnCardOver(int i) {
        if( i < 0 || i >= totalNumberOfCards) { // out of range
            return false;
        }
        if( numFaceUp >= 2) { // at most 2 can be face up
            return false;
        }
        if( isCardFaceUp(i)) { // already face up
            return false;
        }
        if( isCardRemoved(i)) { // already gone
            return false;
        }
        cardStates[i] = FACE_UP;
        numFaceUp++;
        return true;
    }

    public static void main(String[] args) throws Exception {
        GameState gs = new GameState(11, 100);
        gs.show();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while(!gs.isGameOver()) {
            int i;
            do {
                System.out.print("Your Position to flip is: ");
                i = Integer.parseInt(br.readLine());
            } while(!gs.turnCardOver(i));
            gs.show();
            if(gs.readyToMatch()) {
                System.out.println(gs.tryMatch());
                gs.show();
            }
        }
        System.out.println("YOU WON. CONGRATS!");
    }
}
