package net.liucs.genaotd.memorygame;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.CountDownTimer;
import android.os.Handler;
import android.widget.GridLayout;


public class GameActivity extends ActionBarActivity
implements Runnable {

    public static final String LOG_NAME = "Social MemoryGame";

    // Constants for saving/restoring across activities
    private static final String ID_MUSIC_POSITION = "musicPosition";
    private static final String ID_SECONDS_LEFT = "secondsLeft";
    private static final String ID_GAME_STATE = "gameState";
    private int MOVE_EXPIRATION_SECONDS = 4;

    // UI variables
    private Animation unsqueezeAnim;
    private Handler handler = new Handler(); // to delay after showing cards
    private GridLayout tileGrid;
    private TextView countdownText, congratsText;
    private TextView gameTitleText;
    private ImageView cardViews[];
    private int musicPosition;
    private GameMusic gameMusic;
    private boolean gameOver;
    private long secondsLeft;
    private long moveExpiresAt = -1;

    // State of game
    private GameState gameState;
    private CountDownTimer countDownTimer;

    class GameTimer extends CountDownTimer {
        /* Start countdown with 'gameTimeSecs' remaining. */
        GameTimer(long gameTimeSecs) {
            super(gameTimeSecs*1000, 100);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            secondsLeft = millisUntilFinished / 1000;
            countdownText.setText("Countdown Time Left: " + secondsLeft);
            if(secondsLeft <= moveExpiresAt) {
                int[] ups = gameState.getFaceUps();
                gameState.cancelMove();
                moveExpiresAt = -1;
                for(int i : ups) {
                    animateFlipOver(cardViews[i]);
                }
            }
        }

        @Override
        public void onFinish() {
            gameMusic.pause();
            gameMusic.playGameOver(false);
            countdownText.setText("SORRY. GAME OVER!");
            endGame();
        }
    }

    class Clicker implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int i;
            for (i = 0; i < cardViews.length; i++) {
                if (cardViews[i] == v) break;
            }
            Log.i(LOG_NAME, "onClick within " + this);
            Log.i(LOG_NAME, "You clicked card at position #" + i);
            if (gameOver) return;
            if (gameState.turnCardOver(i)) {
                animateFlipOver(v);
                // Redraw and rest of animation is called from onAnimFinish
                if (gameState.readyToMatch()) {
                    moveExpiresAt = -1;
                    handler.postDelayed(GameActivity.this, 2000);
                } else {
                    moveExpiresAt = secondsLeft - MOVE_EXPIRATION_SECONDS;
                }
            }
        }
    }

    public void animateFlingOffScreen(View v, Boolean up) {
        //Animation flingAnim = AnimationUtils.loadAnimation(this, R.anim.fling);
        Animation flingAnim;
        if(up) {
            flingAnim = new TranslateAnimation(0, -150, 0, -1000);
        } else {
            flingAnim = new TranslateAnimation(0, -150, 0, 1000);
        }
        flingAnim.setDuration(1000);
        flingAnim.setAnimationListener(new FlingAnimationHelper());
        v.startAnimation(flingAnim);
    }

    public void animateFlipOver(View v) {
        Animation squeezeAnim = AnimationUtils.loadAnimation(GameActivity.this, R.anim.squeeze);
        squeezeAnim.setAnimationListener(
                new FlipAnimationHelper(v)
        );
        v.startAnimation(squeezeAnim);
    }

    class FlingAnimationHelper implements Animation.AnimationListener {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            redraw();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }

    class FlipAnimationHelper implements Animation.AnimationListener {
        private View view;

        public FlipAnimationHelper(View view) {
            this.view = view;
        }

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            redraw();
            view.startAnimation(unsqueezeAnim);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        gameMusic = new GameMusic(this);
        unsqueezeAnim = AnimationUtils.loadAnimation(GameActivity.this, R.anim.unsqueeze);
        countdownText = (TextView)findViewById(R.id.countdownText);
        congratsText = (TextView)findViewById(R.id.congratsText);
        tileGrid = (GridLayout) findViewById(R.id.tileGrid);
        gameTitleText = (TextView) findViewById(R.id.gameTitleText);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String level = prefs.getString("difficulty_list" , "0");

        if (savedInstanceState != null) {
            gameState = (GameState) savedInstanceState.getSerializable(ID_GAME_STATE);
            musicPosition = savedInstanceState.getInt(ID_MUSIC_POSITION);
            countDownTimer = new GameTimer(savedInstanceState.getLong(ID_SECONDS_LEFT)).start();
        }
        else { // fresh state
            int gameTimeSecs = 500;
            switch(level.charAt(0)) {
                case '1': gameTimeSecs=300; break;
            }
            countDownTimer = new GameTimer(gameTimeSecs).start();
            gameState = new GameState(Integer.parseInt(level), cardFrontDrawables.length);
        }

        tileGrid.setRowCount(gameState.getRows());
        tileGrid.setColumnCount(gameState.getCols());

        Clicker k = new Clicker(); // One listener covers every card
        // Attach clicker to the wrong place
        gameTitleText.setOnClickListener(k);
        cardViews = new ImageView[gameState.getNumCards()];
        for (int i = 0; i < cardViews.length; i++) {
            cardViews[i] = new ImageView(this);
            cardViews[i].setOnClickListener(k);
            tileGrid.addView(cardViews[i]);
        }
        redraw();
    }

    private void redraw() {
        for (int i = 0; i < cardViews.length; i++) {
            if (gameState.isCardRemoved(i)) {
                cardViews[i].setVisibility(View.INVISIBLE);
            } else if(gameState.isCardFaceUp(i)) {
                cardViews[i].setImageResource(cardFrontDrawables[gameState.getCardValue(i)]);
            } else {
                cardViews[i].setImageResource(R.drawable.cardback);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_NAME, "Resuming music at " + musicPosition);
        gameMusic.resume(musicPosition);
    }

    @Override
    protected void onPause() {
        super.onPause();
        musicPosition = gameMusic.pause();
        Log.i(LOG_NAME, "Paused music at " + musicPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countDownTimer.cancel();
        gameMusic.releaseAll();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(LOG_NAME, "Saving instance state.");
        outState.putSerializable(ID_GAME_STATE, gameState);
        outState.putInt(ID_MUSIC_POSITION, musicPosition);
        outState.putLong(ID_SECONDS_LEFT, secondsLeft);
    }


    @Override
    public void run() {
        // Need to grab face-ups before tryMatch!
        int[] ups = gameState.getFaceUps();
        // Do they match?
        if(gameState.tryMatch()) {
            gameMusic.playMatchEffect(true);
            for(int i : ups) {
                // Recreate view so it appears on top.
//                ImageView newView = new ImageView(this);
//                newView.setImageDrawable(cardViews[i].getDrawable());
//                cardViews[i] = newView;
//                tileGrid.removeViewAt(i);
//                tileGrid.addView(cardViews[i], i);
                animateFlingOffScreen(cardViews[i], i*2 < gameState.getNumCards());
            }
        } else {
            gameMusic.playMatchEffect(false);
            // Turn them back over with animation
            for(int i : ups) {
                animateFlipOver(cardViews[i]);
            }
        }
        // Nothing is face up now
        if (gameState.isGameOver()) {
            gameMusic.pause();
            gameMusic.playGameOver(true);
            congratsText.setText("YOU WON. CONGRATS!");
            countDownTimer.cancel();
            endGame();
       }
   }

    // Game is over, either because timer expired or they cleared
    // the cards.
    private void endGame() {
        gameOver = true;
        //finish();
    }

    private int cardFrontDrawables[] = new int[]
    {
            R.drawable.bloglovinblue, R.drawable.bloglovinred,
            R.drawable.chictopiablue, R.drawable.chictopiared,
            R.drawable.etsyblue, R.drawable.etsyred,
            R.drawable.facebookblue, R.drawable.facebookred,
            R.drawable.flickrblue, R.drawable.flickrred,
            R.drawable.foodgawkerblue, R.drawable.foodgawkerred,
            R.drawable.goodreadsblue, R.drawable.goodreadsred,
            R.drawable.googleblue, R.drawable.googlered,
            R.drawable.hellocottonblue, R.drawable.hellocottonred,
            R.drawable.instagramblue, R.drawable.instagramred,
            R.drawable.linkedinblue, R.drawable.linkedinred,
            R.drawable.lystblue, R.drawable.lystred,
            R.drawable.mailblue, R.drawable.mailred,
            R.drawable.pinterestblue, R.drawable.pinterestred,
            R.drawable.rssblue, R.drawable.rssred,
            R.drawable.society6blue, R.drawable.society6red,
            R.drawable.stumbleuponblue, R.drawable.stumbleuponred,
            R.drawable.tumblrblue, R.drawable.tumblrred,
            R.drawable.twitterblue, R.drawable.twitterred,
            R.drawable.vimeoblue, R.drawable.vimeored,
            R.drawable.youtubeblue, R.drawable.youtubered,
            R.drawable.youtubegreen, R.drawable.youtubeblack,
            R.drawable.vimeogreen, R.drawable.vimeoblack,
            R.drawable.twittergreen, R.drawable.twitterblack,
            R.drawable.tumblrgreen, R.drawable.tumblrblack,
            R.drawable.instagramgreen, R.drawable.instagramblack,
            R.drawable.facebookgreen, R.drawable.facebookblack, R.drawable.pinterestgreen, R.drawable.pinterestblack,
            R.drawable.linkedingreen, R.drawable.linkedinblack, R.drawable.stumbleupongreen, R.drawable.stumbleuponblack,
            R.drawable.mailgreen, R.drawable.mailblack, R.drawable.googlegreen,
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
