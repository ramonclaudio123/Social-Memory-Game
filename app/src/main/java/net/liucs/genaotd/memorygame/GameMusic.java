package net.liucs.genaotd.memorygame;

import android.content.Context;
import android.media.MediaPlayer;
import android.provider.MediaStore;

import java.util.ArrayList;

/**
 * Created by league on 4/13/15.
 */
public class GameMusic {

    private ArrayList<MediaPlayer> players = new ArrayList<MediaPlayer>();
    private int idxBackground, idxGoodMatch, idxBadMatch,
                idxGoodGame, idxBadGame;

    public GameMusic(Context context) {
        idxBackground = players.size();
        players.add(MediaPlayer.create(context, R.raw.backgroundmusic));
        players.get(idxBackground).setLooping(false);

        idxGoodMatch = players.size();
        players.add(MediaPlayer.create(context, R.raw.yessir));

        idxBadMatch = players.size();
        players.add(MediaPlayer.create(context, R.raw.ohgod));

        idxGoodGame = players.size();
        players.add(MediaPlayer.create(context, R.raw.goodgamesound));

        idxBadGame = players.size();
        players.add(MediaPlayer.create(context, R.raw.badgamesound));
    }

    public void releaseAll() {
        for(MediaPlayer p : players) {
            p.release();
        }
    }

    public void playMatchEffect(boolean success) {
        players.get(success? idxGoodMatch : idxBadMatch).start();
    }

    public void playGameOver(boolean success) {
        players.get(success? idxGoodGame : idxBadGame).start();
    }

    public int pause() {
        players.get(idxBackground).pause();
        return players.get(idxBackground).getCurrentPosition();
    }

    public void resume(int position) {
        players.get(idxBackground).seekTo(position);
        players.get(idxBackground).start();
    }
}
