package net.liucs.genaotd.memorygame;

import junit.framework.TestCase;

/**
 * Created by league oblin 4/20/15.
 */
public class GameStateTest extends TestCase {

    private GameState gs;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gs = new GameState(0, 32);
    }

    public void testSomething() {
        // After clicking two cards, we are ready to match.
        assertTrue(gs.turnCardOver(0));
        assertTrue(gs.turnCardOver(1));
        assertTrue(gs.readyToMatch());
    }

    public void testAnother() {
        // Clicking the same card twice is not allowed.
        assertTrue(gs.turnCardOver(0));
        assertFalse(gs.turnCardOver(0));
    }
}
